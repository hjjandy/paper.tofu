\section{Design} \label{sec:design}

%%%%%%%%%%%%%%%%%%%%%%%
\lstdefinestyle{language-keywords}{
  keywords=[2]{inflate, findViewById, assignedFrom, call},
  keywordstyle=[2]\color{blue},
}
%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[t]
\centering
\begin{tikzpicture}[auto,node distance=5pt]
%\draw[help lines, draw] (-4, -4) grid (4, 4);
\node at (-4,3) [fill=gray!20,draw,rounded corners,inner sep=2pt, font=\footnotesize] (app) {App};
\node at (-4,2) [fill=red!20, circle, inner sep=1pt,font=\footnotesize] (user) {User};
\node at (-3,3) [anchor=west,draw,align=center, inner sep=2pt,inner xsep=4pt,font=\footnotesize] (dis) {UI Element\\ Discovery};
    \node at (-1.3,3.4) [fill=white, draw, circle, font=\small,inner sep=1pt] {0};
\node at (-3,2) [anchor=west,draw,double,align=center, inner sep=2pt,inner xsep=4pt,font=\footnotesize] (uitracking) {UI Element\\ Tracking};
    \node at (-1.3,2.4) [fill=white, draw, circle, font=\small,inner sep=1pt] {1};
\node at (-0.6,2) [anchor=west,draw,double,align=center, inner sep=2pt,font=\footnotesize] (bwd) {Backward\\ Data Tracking};
    \node at (1.2,2.4) [fill=white, draw, circle, font=\small,inner sep=1pt] {2};
\node at (-0.6,3) [anchor=west,draw,double,align=center, inner sep=2pt,font=\footnotesize] (fwd) {Forward\\ Data Tracking};
    \node at (1.2,3.4) [fill=white, draw, circle, font=\small,inner sep=1pt] {3};
\node at (2,3) [anchor=west,draw,double,dashed,align=center, inner sep=2pt,font=\footnotesize] (rm) {Code\\ Removal};
    \node at (3.2,3.4) [fill=white, draw, circle, font=\small,inner sep=1pt] {4};
\node at (2,2) [fill=blue!20,anchor=west,draw,dashed,rounded corners,align=center,inner sep=2pt,font=\footnotesize] (mapp) {Modified\\ App};
%%%%% paths
\path [draw, -latex] (app) to (dis);
\path [draw, -latex] (user) to (dis.south west);
\path [draw, -latex] (dis) to (uitracking);
\path [draw, -latex] (uitracking) to (bwd);
\path [draw, -latex] (bwd) to (fwd);
\path [draw, -latex] (fwd) to (rm);
\path [draw, -latex] (rm) to (mapp);
\end{tikzpicture}
\vspace{-0.1in}
\caption{Approach overview.}
\label{fig:overview}
\vspace{-0.15in}
\end{figure}

In this section, we will discuss our UI driven approach to discovering  
removable code elements in Android apps.  
Figure~\ref{fig:overview} shows the work flow of our approach. 
As discussed in Section~\ref{sec:motivation}, after receiving the user 
specified UI elements and discovering the program locations referring to 
them (step \stepn{0}), our approach takes four steps to 
detect potentially removable code elements:
\stepn{1} forwardly discovers all uses of the specified UI elements;
\stepn{2} backwardly tracks the data used in the UI elements;% to its 
%generation points in background functionalities; and the program 
%locations firing the background tasks;
\stepn{3} finds all uses of the data, starting from the discovered data generation points; 
\stepn{4} iteratively removes the code elements based on the results of
data tracking.
Steps \stepn{0} and \stepn{1} are discussed in 
Section~\ref{sec:sub:turnoffui}, Section~\ref{sec:sub:bg} describes 
steps \stepn{2} and \stepn{3}, and Section~\ref{sec:sub:rm} talks about
step \stepn{4}.

\begin{figure}[tb]
\centering
\lstdefinestyle{design-code}{
	keywords=[2]{data, ck},
	keywordstyle=[2]\color{blue},
	keywords=[3]{id0, id1, id2},
	keywordstyle=[3]\color{mauve},
    moredelim=**[is][{\btHL[draw=black, rectangle, dashed, rounded corners=0, fill=none]}]{?}{?},
}
\begin{lstlisting}[language=Java,style=customlst,style=design-code,style=HL,escapechar=|]
// class A declares field "data"
class B extends A {
  a() { ^t = new BgTask(this);trigger(t);^  |\label{line:design:triggerB}| }
  b() { textView0 = findViewById(id0);//unwanted |\label{line:design:findviewB}|
         |\space|@textView0.setText(data);@  |\label{line:design:settextB}| }  }//``B''
class C extends A {
  c() { ^g = new BgTask(this);trigger(g);^  |\label{line:design:triggerC}| }
  d() { textView1 = findViewById(id1);//unwanted |\label{line:design:findviewC1}|
         |\space|@textView1.setText(data.split(":")[0]);@  |\label{line:design:settextC1}|
          textView2 = findViewById(id2);//not unwanted |\label{line:design:findviewC2}|
         |\space|@textView2.setText(data.split(":")[1]);@  |\label{line:design:settextC2}|} }//``C''
class BgTask { A ck; BgTask(A a) {ck = a;}
  void run() { *ck.data = getInputData();* |\label{line:design:getinput}|}}//``BgTask''
\end{lstlisting}
\vspace{-0.1in}
\caption{Code example for discussion.}
\label{fig:design-code}
\vspace{-0.1in}
\end{figure}

We use the code snippet in Figure~\ref{fig:design-code} throughout the 
section to exemplify the approach details. 
Assume that {\tt id0} and {\tt id1} refer to unwanted UI elements but
{\tt id2} doesn't. {\tt BgTask} represents a background functionality that 
obtains input data from remote servers and stores the result to {\tt data}.
Line~\ref{line:design:getinput} abstracts the operations of obtaining input 
data by function \texttt{getInputData}. A concrete example is 
\texttt{HttpClient.execute()} which is commonly used in Android apps to 
fetch remote data from Web servers.
The data acquisition task is initiated at two locations: line~\ref{line:design:triggerB} and 
line~\ref{line:design:triggerC}. The data is displayed on UI elements at 
lines~\ref{line:design:settextB}, \ref{line:design:settextC1} and 
\ref{line:design:settextC2}.

\begin{figure}[t]
\centering\small
\newcommand{\lcom}[1]{\hfill{/*\it #1}*/}%comment in line%
\setlength\tabcolsep{3pt}
\begin{tabular}{|l c r p{6cm}|} \hline 
Program     & P & ::=   & K*  \\
Class       & K & ::=   & M*  \\
Method      & M & ::=   & m(x) \{s*\}  \\
Statement   & s & ::=   & x :=$^l$ c {\lcom{constant}} \\
            &   & |     & x :=$^l$ inflate(i) \lcom{inflation} \\
            &   & |     & x :=$^l$ findViewById(i) \lcom{get view}\\
            &   & |     & x :=$^l$ $\ominus$y \lcom{unary assignment} \\
            &   & |     & x :=$^l$ y$\oplus$z \lcom{binary assignment} \\
            &   & |     & x :=$^l$ $\phi$(y, z) \lcom{value merging in SSA} \\
            &   & |     & x :=$^l$ y.z \lcom{get field} \\
            &   & |     & x.y :=$^l$ z \lcom{put field} \\
            &   & |     & x :=$^l$ checkcast(y) \lcom{type cast} \\
            &   & |     & x :=$^l$ new k(y) \lcom{new instance} \\
            &   & |     & x :=$^l$ y.m(z) \lcom{method call}  \\
            &   & |     & return$^l$(x) \lcom{return in a method} \\
Variable    & \multicolumn{2}{l}{x, y, z} & \lcom{all variables}    \\
ID          & i &       & \lcom{UI-related Id} \\
Value       & c &       & \lcom{non-Id constant} \\ 
Label       &$l$&       & \lcom{\{$l_1$, $l_2$, $l_3$, ...\}} \\ \hline
\end{tabular}
\caption{Simplified language model.}
\label{fig:language}
\vspace{-0.2in}
\end{figure}

\subsection{Language Abstraction} \label{sec:sub:lang}

%\jianjun{add if/switch into the language??}
To simplify our discussion, we introduce an abstract language,
as presented in Figure~\ref{fig:language}. A program is made up 
of classes, a class contains a list of methods, and each method
contains a number of statements. We model common types of 
statements and other operations are abstracted away or simplified. 
We label statements with a superscript.
 
As we discussed in Section~\ref{sec:motivation}, we start our 
analysis from the layout inflations and aim to remove the inflated views. 
%. The inflated views are what 
%we want to remove. 
Thus, we introduce the function {\it inflate} to represent all kinds of inflations in the code 
(\eg lines~\ref{line:wbn:inflatefragment} and \ref{line:wbn:inflateadapter}
in Figure~\ref{fig:wb-code}).
Additionally, we introduce another function {\it findViewById}  to
represent the operation of an app looking for a UI element. 

%We introduce a function {\it inflate}
%to represent all kinds of inflations in the code, \eg 
%line~\ref{line:wbn:inflatefragment} and line~\ref{line:wbn:inflateadapter}
%. In addition, when the app tries to 
%put data to the UI element, it first retrieves the corresponding 
%element via {\tt findViewById}. Thus we use a function {\it findViewById} to represent the element retrieval. Both functions 
%take a UI-related ID as input and return the corresponding views.

We abstract all types of assignment, such as unary assignment,
binary assignment, 
%$\phi$ assignment in single static assignment (SSA) form,
type cast assignment and assignment from/to a field variable. 
Note that our language is a kind of 
single static assignment (SSA) language such that conditionals (including loops) are implicitly represented by the value merge statement $\phi(y,z)$. 
Since the predicate of the value merge statement is irrelevant in our analysis,
it is abstracted away and hence $y$ and $z$ denote the values of the same 
variable in the two branches of a conditional. 

For object creation (\ie new instance statement)
and method calls, we assume there is only one parameter besides the receiver
object (`{\tt this}') of an instance method call. A return statement returns
a value from a callee method to a variable in the caller method.


\begin{figure}[t]
\centering\small
\begin{tabular}{|ccl c ccl|} \hline
\arrayof{s} & := & statementsIn(m) & & \arrayof{m} & := & methodsIn(k) \\
x & := & thisOf(k) &  & x & := & paramOf(m) \\
s & := & returnOf(m) & & & & \\
	\hline
\end{tabular}
\caption{Functions for {\it class}, {\it method} and {\it statement}.}
\label{fig:relation-cms}
\vspace{-0.2in}
\end{figure}

We also define a number of auxiliary functions for statements, methods, and classes 
to acquire correlated information during analysis. 
These functions are shown 
in Figure~\ref{fig:relation-cms}. Function {\it statementsIn}
returns all statements inside a method, represented by \arrayofTT{s}.
Similarly, {\it methodsIn} returns all declared methods in a class {\tt k}.
Functions {\it thisOf} and {\it paramOf} behave similarly, except that 
one looks for `{\tt this}' reference in the callee method and the other
searches the corresponding formal parameter.
The return statement of a method is given by {\it returnOf}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Rules for UI %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% [ moved here for formatting ] %%%%%%%
\begin{figure*}[ht]
\centering\footnotesize%\small
\begin{tabular}{|c|} \hline 
\begin{minipage}{3.15in}
\vspace{1mm}
\inference[UI-Inflate]{belongToSpecifiedUI(i)}
    {\Gamma_1, \{x :=^l inflate(i)\}_\epsilon |= \Gamma_1 => [x_\epsilon, l_\epsilon : \typeUI]\Gamma_1}
\vspace{2mm}

\inference[UI-FindView]{belongToSpecifiedUI(i)}
    {\Gamma_1, \{x :=^l findViewById(i)\}_\epsilon |= \Gamma_1 => [x_\epsilon, l_\epsilon : \typeUI]\Gamma_1}
\vspace{2mm}

\inference[UI-Call-This]{nonApi(m) &
    \Gamma_1 |- y_\epsilon : \typeUI &
    \epsilon' = \epsilon \cdot l &
    t = thisOf(m) }
	{\Gamma_1, \{x :=^l y.m(z)\}_\epsilon |= \Gamma_1 => [x_\epsilon, t_{\epsilon'}, l_\epsilon : \typeUI]\Gamma_1}
\vspace{2mm}

%\inference[UI-Call-Arg]{nonApi(m) &
%    \Gamma_1 |- z_\epsilon : \typeUI &
%    \epsilon' = \epsilon \cdot l &
%    p = paramOf(m) }
%    {\Gamma_1, \{x :=^l y.m(z)\}_\epsilon |= \Gamma_1 => [p_{\epsilon'} : \typeUI]\Gamma_1}
%\vspace{2mm}

\inference[UI-BAssign]{\Gamma_1 |- y_\epsilon : \typeUI }
    {\Gamma_1, \{x_\epsilon :=^l y_\epsilon \oplus z_\epsilon\}_\epsilon |= \Gamma_1 => [x_\epsilon, l_\epsilon : \typeUI]\Gamma_1}
\vspace{2mm}

\inference[UI-Return]{\Gamma_1 |- r_{\epsilon'} : \typeUI &
    \epsilon' = \epsilon \cdot l & 
    x :=^l y.m(z) }
    {\Gamma_1, \{return^{l'}(r)\}_{\epsilon'} |= \Gamma_1 => [x_\epsilon, l'_{\epsilon'}, l_\epsilon : \typeUI]\Gamma_1}

\vspace{1mm}
\end{minipage} \\ \hline
\end{tabular}
\caption{Rules for discovering code elements associated with the UI elements.}
\label{fig:rule-ui}
\vspace{-0.2in}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Type System} \label{sec:sub:typesys}

We formalize our approach in a type system. Code elements, including variables,
statements, methods, and classes are associated with tags.
A tag is treated as the type $T$ of the code element $l$.
We say $l$ has type $T$, written as $l : T$. The type of a code element
may be a set of tags, for example, $l : \{T, T'\}$. In this case,  
We use ``$\cup$'' to union two sets of types together.
In this paper, we define the type domain as:
\[\footnotesize Type = \left\{ \begin{array}{llll}
\typeUI & \typeData    & \typeInput      \\
\typeR  & \typeU  \end{array} \right\} \]

Type \typeUI is used to mark the variables or statements that depend on
the specified UI elements. We type the code elements with it and propagate 
it at step \stepn{1}. For example, we type an
{\tt inflate()} method call where the inflated view is a user specified
UI element and the resultant variable with 
\typeUI. When we find any data uses on the specified UI elements, 
we type the data variable with \typeData and then backwardly propagate
it at step \stepn{2}. If we reach a data generation point along  
above propagation, we get the knowledge that the data 
is some input data from outside sources. We type the data generation 
point and the data variable with \typeInput and propagate it forwardly
(step \stepn{3}). Eventually, we can mark which code elements are 
removable or definitely unremovable using the corresponding types (step
\stepn{4}).

Notice that type \typeInput can be parameterized with a data generation point. It will help us distinguish the types of code 
elements which are correlated with multiple data generation points.

In addition to the concrete types, if any code element has not been visited 
or is not interesting to us, we say it is not typed, in the notation of 
{\it nil}. 

Consider that the same code element executed in different calling
contexts may produce different results, and in some contexts 
it may be related to the specified UI elements while in other 
contexts it may not be. We use $\epsilon$ to represent the calling 
context which is a stack of labels referring to method calls or new
instance sites. Each code element $l$ is tagged with a context 
(\eg $l_\epsilon$) in order to conduct context-sensitive analysis. 
Different contexts are depicted by the subscript of $\epsilon$, 
\eg $\epsilon_1$ and $\epsilon_2$. 
We use ``$\cdot$'' to concatenate a context with a label to form a new 
context for the statements in the callee method associated with $l$, 
for example, $\epsilon' := \epsilon \cdot l$.

The mappings from the code elements to the types form the context $\Gamma$
of the type system, which is iteratively updated during analysis
until a fixed point is reached. For example, at the beginning, 
$\Gamma$ is empty. Upon a removable statement $l_\epsilon$, $\Gamma$
is updated to \{$l_\epsilon$ : \typer\}. At this point, we have 
$\Gamma |- l_\epsilon$ : \typeR, which means under  (type) context $\Gamma$, 
statement $l_\epsilon$ is typed with \typeR. In other words, 
$\Gamma(l_\epsilon)$ = \typeR,  where $\Gamma(l_\epsilon)$ evaluates 
statement $l_\epsilon$ in the context to obtain the corresponding type.

When a statement $l_\epsilon$ is evaluated, the context may be 
updated. We use $\Gamma, l_\epsilon |= \Gamma => \Gamma'$ to indicate that
under type context $\Gamma$, evaluating code 
$l_\epsilon$ updates the context from $\Gamma$ to $\Gamma'$.  
We use $[l_\epsilon : T]\Gamma$ to represent an update to the context.
Specifically, if no mapping is found for $l_\epsilon$ in $\Gamma$, the
mapping is added to the context. But if there exists a mapping
for $l_\epsilon$, the rule substitutes the existing type of $l_\epsilon$ with
type $T$. Multiple mappings can be updated simultaneously. For
instance, $[l_{\epsilon_1} : T, l'_{\epsilon_2} : T']\Gamma$ update 
the context for two code elements $l_{\epsilon_1}$ and $l_{\epsilon_2}$.
We also use $[l_{\epsilon_1}, l'_{\epsilon_2} : T]\Gamma$ to denote 
$[l_{\epsilon_1} : T, l'_{\epsilon_2} : T]\Gamma$ for brevity.

In the following analysis, we define four type contexts: $\Gamma_1$, 
$\Gamma_2$, $\Gamma_3$ and $\Gamma_4$ for the four steps respectively. 
We also have a special context $\mathit{MR}$ in which the discovered 
data generation points are mapped to {\it True} or {\it False}, indicating whether 
the corresponding data generation points must be retained or not.


\subsection{Turning Off UI Element} \label{sec:sub:turnoffui}

In this section, we discuss how to identify the code elements related 
to the unwanted UI elements, \ie steps \stepn{0} and 
\stepn{1} in Figure~\ref{fig:overview}. Starting from {\it inflate}
and {\it findViewById}, we forwardly track the uses of the specified
UI elements and type all correlated statements and variables with \typeUI. 

We define the rules in Figure~\ref{fig:rule-ui}. We omit
the rules for some statements, \eg unary assignment, 
$\phi$ assignment and field access, due to the space limit.
In addition, \colorchange{red}{we do not present the rules for API method calls that require models.} 
New instance operations behave similarly to
method calls and thus we omit the corresponding rules too.

Based on the language definition, 
the user specified UI elements are introduced into the code through layout 
inflation ({\it inflate}) or view finding ({\it findViewById}). 
We apply rules \rulename{UI-Inflate} and \rulename{UI-FindView} to start the analysis. 
If the given id is corresponding to a specified UI element, we say
the resultant variable and the statement are related to specific UI element. 

If the receiver object {\tt y} of a method call {\tt m} is typed with \typeUI,
the definition of {\tt y} will be \emph{potentially} 
removed. The removal results in a null reference, leading to run-time 
exceptions. Therefore, we apply rule \rulename{UI-Call-This} in this case to
type the resultant variable {\tt x}, the method call statement $l$ and 
%For any method call in which the receiver object is typed with
%\typeUI, the resultant variable, as well as the method call statement and 
the corresponding `{\tt this}' reference in the callee method with \typeUI.
%have the same type (UI-Call-This). 
%{\bf [XZ: I don't understand your point in the following sentence]} 
%\jianjun{Revised. See above statement.}
%\st{Our intuition of directly typing the resultant variable {\tt x} is that 
%the definition of the receiver object {\tt y} will probably be removed and
%hence {\tt y} comes to be a null pointer, causing runtime errors.}
%%%%%%%%%%%%%
If an actual argument {\tt z} is typed, the behavior is similar to 
\rulename{UI-Call-This} and the corresponding rule is omitted.
%%%%%%%%%%%%%
%\st{If the argument is typed, we only propagate the type of the argument 
%to the corresponding parameter in the callee method (UI-Call-Arg) without
%typing the resultant variable as the result of the method call may have nothing to do with
%any UI elements. Different from rule UI-Call-This,} {\bf [XZ: again, I don't get your point]}\jianjun{remove UI-Call-Arg and descriptions. Treat `this' and normal arg as the same.} \st{reference
%to undefined variable {\tt z} can be replaced with 0.} 
Rule \rulename{UI-BAssign} 
indicates that if a right-hand-side variable is typed, 
%can 
%be slightly modified for all other types of assignment, simply typing
the left-hand-side variable and the statement are typed too.
Rule \rulename{UI-Return} is applied when 
a return variable in a callee method is typed with \typeUI. It propagates
the type to the resultant variable at the corresponding call site and 
types the call site with \typeUI. 

{\bf Example:} Consider applying the rules to the code snippet in Figure~\ref{fig:design-code}.
First, we type {\tt textView0} and {\tt textView1},  
lines~\ref{line:design:findviewB} and \ref{line:design:findviewC1} with
\typeUI by rule \rulename{UI-FindView}. Then through type propagation, 
lines~\ref{line:design:settextB} and \ref{line:design:settextC1} are typed 
with \typeUI. We present the context updates as follows.
$\epsilon_1$ is the calling context of method {\tt B.b()} and 
$\epsilon_2$ is the calling context of method {\tt C.d()}.

{\footnotesize
\[\arraycolsep=3pt \mbox{UI-FindView(Line~\ref{line:design:findviewB}$_{\epsilon_1}$, Line~\ref{line:design:findviewC1}$_{\epsilon_2}$)} => \left[ \begin{array}{rcl}
    \mbox{textView0$_{\epsilon_1}$} & : & \typeUI \\
    \mbox{textView1$_{\epsilon_2}$} & : & \typeUI \\
    \mbox{Lines~\ref{line:design:findviewB}$_{\epsilon_1}, $\ref{line:design:findviewC1}$_{\epsilon_2}$} & : & \typeUI\\
%    \mbox{Line~\ref{line:design:findviewC1}$_{\epsilon_2}$} & : & \typeUI
\end{array} \right] \Gamma_1 \]
\[\arraycolsep=3pt \mbox{UI-Call-This(Line~\ref{line:design:settextB}$_{\epsilon_1}$, Line~\ref{line:design:settextC1}$_{\epsilon_2}$)} => \left[ \begin{array}{rcl}
    \mbox{Lines~\ref{line:design:settextB}$_{\epsilon_1}$, \ref{line:design:settextC1}$_{\epsilon_2}$} & : & \typeUI\\
%    \mbox{Line~\ref{line:design:settextC1}$_{\epsilon_2}$} & : & \typeUI
\end{array} \right] \Gamma_1 \] \vspace{-4mm}
\vspace{-0.1in}
}

\subsection{Discovering Associated Background Functionalities} \label{sec:sub:bg}


\begin{figure*}[ht]
\centering\footnotesize%\small
\begin{tabular}{|c|} \hline 
\begin{minipage}{3.15in}
\vspace{1mm}

\inference[UI-Put-Data]{\Gamma_1 |- y_\epsilon : \typeUI &
    \Gamma_1 |- l_\epsilon : \typeUI &
    apiPutDataToUI(m)}
    {\Gamma_1, \{x :=^l y.m(z)\}_\epsilon |= \Gamma_2 => [z_\epsilon : \typeData]\Gamma_2}
\vspace{2mm}

\inference[Bwd-Assign]{\Gamma_2 |- x_\epsilon : \typeData}
    {\Gamma_2, \{x :=^l y \oplus z\}_\epsilon |= \Gamma_2 => [y_\epsilon, z_\epsilon, l_\epsilon : \typeData]\Gamma_2}
\vspace{2mm}

\inference[Bwd-Call-Return]{\Gamma_2 |- x_\epsilon : \typeData &
    nonApi(m) & 
    \epsilon' = \epsilon \cdot l  & 
    \{return^{l'}(r)\} = returnOf(m) }
    {\Gamma_2, \{x :=^l y.m(z)\}_\epsilon |= \Gamma_2 => [r_{\epsilon'}, l'_{\epsilon'}, l_\epsilon : \typeData]\Gamma_2}
\vspace{2mm}

\inference[Bwd-Call-Param]{nonConstructor(m) & 
    \epsilon' = \epsilon \cdot l  &
    p = paramOf(m) & 
    \Gamma_2 |- p_{\epsilon'} : \typeData   }
    {\Gamma_2, \{x :=^l y.m(a)\}_\epsilon |= \Gamma_2 => [a_\epsilon : \typeData]\Gamma_2} 
\vspace{2mm}

%\inference[Bwd-Call-Param-2]{\Gamma |- y'_{\epsilon'} : \typeData &
%    m = constructorOf(k) &
%    \epsilon = popTop(\epsilon') & 
%    \{\underline{x_\epsilon :=^l new~k(y_\epsilon)}\}_\epsilon = topOf(\epsilon') }
%    {\Gamma, \{m^{l'}(y_{\epsilon'})\{...\}\}_{\epsilon} |= \Gamma => [y_\epsilon : \Gamma(y_\epsilon) \cup \typeData, l_\epsilon : \Gamma(l_\epsilon) \cup \typeDataRelated]\Gamma} 
%\vspace{2mm}

%\inference[Bwd-Call-API-1]{\Gamma |- x_\epsilon : \typeData &
%    apiNonGetInputData(m)}
%    {\Gamma, \{x_\epsilon :=^l y_\epsilon.m(z_\epsilon)\}_\epsilon |= \Gamma => [y_\epsilon : \Gamma(y_\epsilon) \cup modelBwd(m, x_\epsilon, y_\epsilon), z_\epsilon : \Gamma(z_\epsilon) \cup modelBwd(m, x_\epsilon, z_\epsilon), l_\epsilon : \Gamma(l_\epsilon) \cup \typeDataRelated]\Gamma}
%\vspace{2mm}

%\inference[Bwd-Call-API-2]{\Gamma |- y_\epsilon : \typeData &
%    apiNonGetInputData(m) }
%    {\Gamma, \{x_\epsilon :=^l y_\epsilon.m(z_\epsilon)\}_\epsilon |= \Gamma => [z_\epsilon : \Gamma(z_\epsilon) \cup modelBwd(m, y_\epsilon, z_\epsilon), l_\epsilon : \Gamma(l_\epsilon) \cup \typeDataRelated]\Gamma}
%\vspace{2mm}
\end{minipage}\\ \hline
\end{tabular} 
\caption{Rules for backwardly discovering data relevant code elements.}
\label{fig:rule-backward}
\vspace{-0.1in}
\end{figure*}

\begin{figure*}[ht]
\centering\footnotesize%\small
\begin{tabular}{|c|} \hline 
\begin{minipage}{3.15in}
\vspace{1mm}
\inference[Bwd-Call-Data-Gen]{\Gamma_2 |- x_\epsilon : \typeData &
    apiGetInputData(m)  }
    {\Gamma_2, \{x :=^l y.m(z)\}_\epsilon |= \Gamma_3=> [x_\epsilon , l_\epsilon : \Gamma_3(l_\epsilon) \cup \{\typeInput(l)\}]\Gamma_3}
\vspace{2mm}

%\inference[Bwd-DG-Trigger]{\Gamma |- l_\epsilon : \typeDataP &
%    apiGetInputData(m) &
%    \epsilon' = popUntilTopIsTriggerSite(\epsilon) & 
%    l'_{\epsilon'} = topOf(\epsilon') }
%    {\Gamma, \{x_\epsilon :=^l y_\epsilon.m(z_\epsilon)\}_\epsilon | = \Gamma => [l'_{\epsilon'} : \Gamma(l'_{\epsilon'}) \cup \typeTrigger]\Gamma}
%\vspace{2mm}

\inference[Fwd-Assign]{\typeInput(l^d) \in \Gamma_3(y_\epsilon) }
    {\Gamma_3, \{x :=^l y \oplus z\}_\epsilon |= \Gamma_3 => [x_\epsilon, l_\epsilon : \Gamma_3(x_\epsilon) \cup \{\typeInput(l^d)\}]\Gamma_3}
\vspace{2mm}

\inference[Fwd-Call-This]{nonApi(m) &
    \typeInput(l^d) \in \Gamma_3(y_\epsilon) &
    \epsilon' = \epsilon \cdot l &
    t = thisOf(m) }
    {\Gamma_3, \{x :=^l y.m(z)\}_\epsilon |= \Gamma_3 => [x_\epsilon , t_{\epsilon'}, l_\epsilon : \Gamma_3(x_\epsilon) \cup \{\typeInput(l^d)\}]\Gamma_3}
\vspace{2mm}

%\inference[Fwd-Call-Arg]{nonApi(m) &
%    \typeInput(l^d) \in \Gamma_3(a_\epsilon) &
%    \epsilon' = \epsilon \cdot l & 
%    p = paramOf(m) }
%    {\Gamma_3, \{x :=^l y.m(a)\}_\epsilon |= \Gamma_3 => [p_{\epsilon'} : \Gamma_3(p_{\epsilon'}) \cup \{\typeInput(l^d)\}]\Gamma_3}
%\vspace{2mm}

\inference[Fwd-Call-Return]{\typeInput(l^d) \in \Gamma_3(r_{\epsilon'}) &
    \epsilon' = \epsilon \cdot l & 
    x :=^l y.m(z) }
    {\Gamma_3, \{return^{l'}(r_{\epsilon'})\}_{\epsilon'} |= \Gamma_3 => [x_\epsilon, l_\epsilon, l'_{\epsilon'} : \Gamma_3(x_{\epsilon}) \cup \{\typeInput(l^d)\}]\Gamma_3}
\vspace{2mm}

%\inference[Fwd-New]{\Gamma |- y_\epsilon : \typeInput^n &
%    m = constructorOf(k) &
%    nonFrameworkClass(k) &
%    \epsilon' = \epsilon + l_\epsilon & 
%    y'_{\epsilon'} = paramOf(m_\epsilon) }
%    {\Gamma, \{x_\epsilon :=^l new~k(y_\epsilon)\}_\epsilon |= \Gamma => [y'_{\epsilon'} : \Gamma(y'_{\epsilon'}) \cup \typeInput^n]\Gamma}
%\vspace{2mm}

\inference[{\bf Unexpected-Data-Use}]{\typeInput(l^d) \in \Gamma_3(z_\epsilon) &
    \typeUI \notin \Gamma_1(y_\epsilon) &
    apiPutDataToUI(m) }
    {\Gamma_1, \Gamma_3, \{x :=^l y.m(z)\}_\epsilon |= MR(l^d) -> True}
\vspace{1mm}

\end{minipage}\\ \hline
\end{tabular} 
\caption{Rules for discovering the uses of input data.}
\label{fig:rule-forward}
\vspace{-0.2in}
\end{figure*}

If we reach some API method calls that put data to the specified UI elements for 
display, we track where the data is generated.
We {\it backwardly} track the generation of the data and type all the 
involved statements and associated variables with \typeData 
(step \stepn{2}). 
When we reach a data generation point that acquires data from outside 
sources (\eg {\tt HttpClient.execute()}), we use \typeInput to type the 
data variable and the statement, indicating
their correlation with input data. We then {\it forwardly} 
propagate \typeInput along data flows (step \stepn{3}). 
Along the forward propagation, 
%\jianjun{rm the context of data gen point}
we parameterize \typeInput with $l^d$, a data generation point, to distinguish
data originating from different points.
% at $l^d$, %in calling 
%context $\xi$, 
%as an input of \typeInput, to differentiate the 
%variants of \typeInput, which are associated with different data generation points.
If we encounter any cases in which the tracked data is used in some UI 
components other than the unwanted ones, we need to remember that the 
corresponding data generation points from which the data propagations 
are unremovable.
We define the backward propagation rules in Figure~\ref{fig:rule-backward} 
and the forward rules in Figure~\ref{fig:rule-forward}. 

Step \stepn{2} starts with the discovery of API calls putting data on specified
UI elements. Rule \rulename{UI-Put-Data} initiates the context $\Gamma_2$ which
stores mappings from variables or statements to type \typeData, by 
typing the data variable with \typeData. \rulename{Bwd-*} rules are then applied 
to propagate the type backwardly. If a resultant variable {\tt x} is 
typed in a method call, the corresponding return value {\tt r} in 
the callee method should also be typed (\rulename{Bwd-Call-Return}).
If a formal parameter {\tt p} in a callee method is typed, the corresponding 
actual argument at the caller is typed 
(\rulename{Bwd-Call-Param}). If the typed variable is {\tt this} reference in the 
callee method, we type the corresponding receiver object.
%The propagation is the same as that for a normal parameter and we omit the
%rule.

During the propagation, if we meet a API method call that acquires outside
data and stores the data to any variables under tracking, we consider 
the method call as a data generation point and thus type the data variable
and the data generation point with \typeInput in context $\Gamma_3$ 
(\rulename{Bwd-Call-Data-Gen}).
Given the fact that a code element may be correlated with multiple data
generations, we associate each type \typeInput with the location of 
data generation and we use a set to represent the type of a code element.
For example, statement {\tt a = b $\cdot$ c} concatenates two strings and {\tt b}
is generated by a method call at $l^0$ while {\tt c} is generated
at $l^1$. Therefore, {\tt a} has a type of 
\{\typeinput($l^0$, $l^1$)\}. 
%\jianjun{rm ctx of DG point}
If either one of the data 
generation points cannot be removed, this statement is unremovable.
The forward propagation rules are straightforward, similar to 
the ones for discovering UI related code elements, except that 
type \typeInput is parameterized with a data generation point 
%as input 
and the resultant type is a union of the incoming type and the original types.

If the input data is discovered to be used by some UI elements that 
are not correlated with the unwanted UI elements, we apply rule
\rulename{Unexpected-Data-Use} and mark in context $\mathit{MR}$ that the 
corresponding data generation point must be retained, \ie unremovable.

{\bf Example:} We apply the rules to the example in 
Figure~\ref{fig:design-code}. Backward data tracking updates
$\Gamma_2$ as:

{\footnotesize \vspace{-1mm}
\[\arraycolsep=3pt \left[ \begin{array}{rcl}
\mbox{data$_{\{...;\ref{line:design:triggerB}\}}$, data$_{\{...;\ref{line:design:triggerC}\}}$}, 
\mbox{Line~\ref{line:design:getinput}$_{\{...;\ref{line:design:triggerB}\}}$, Line~\ref{line:design:getinput}$_{\{...;\ref{line:design:triggerC}\}}$} & : & \typeData 
\end{array} \right] \Gamma_2 \] 
\vspace{-0.15in}
}

%{\footnotesize
%\[\arraycolsep=3pt \left[ \begin{array}{rcl}
%\mbox{data$_{\{...;\ref{line:design:triggerB}\}}$, data$_{\{...;\ref{line:design:triggerC}\}}$} & : & \typeData \\
%\mbox{Line~\ref{line:design:getinput}$_{\{...;\ref{line:design:triggerB}\}}$, Line~\ref{line:design:getinput}$_{\{...;\ref{line:design:triggerC}\}}$} & : & \typeData 
%\end{array} \right] \Gamma_2 \] 
%}

We use {\footnotesize \{...;n\}} to denote the calling context of method {\tt run()} where
{\it n} denotes the line number of corresponding trigger site. The background 
functionality is triggered in two contexts, one at 
line~\ref{line:design:triggerB} and the other at 
line~\ref{line:design:triggerC}.
This results in different instances of variable {\tt data} and the statement of getting input data.
Therefore, variable {\tt data} at line~\ref{line:design:getinput} is 
defined in two calling contexts, represented by 
$\mathit{data}_{\{...;\ref{line:design:triggerB}\}}$ and 
$\mathit{data}_{\{...;\ref{line:design:triggerC}\}}$. 
Starting from the data generation points, we have
%\jianjun{rm ctx of DG}

{\footnotesize
\[\arraycolsep=2pt \left[ \begin{array}{rcl}
	\mbox{data$_{\{...;\ref{line:design:triggerB}\}}$, Line~\ref{line:design:getinput}$_{\{...;\ref{line:design:triggerB}\}}$, Line~\ref{line:design:settextB}$_{\epsilon_1}$} & : & \{\typeInput(\mbox{Line}~\ref{line:design:getinput})\} \\
	\mbox{data$_{\{...;\ref{line:design:triggerC}\}}$, Line~\ref{line:design:getinput}$_{\{...;\ref{line:design:triggerC}\}}$} & : & \{\typeInput(\mbox{Line}~\ref{line:design:getinput})\} \\
	\mbox{Line~\ref{line:design:settextC1}$_{\epsilon_2}$, , Line~\ref{line:design:settextC2}$_{\epsilon_2}$} & : & \{\typeInput(\mbox{Line}~\ref{line:design:getinput})\} 
\end{array} \right] \Gamma_3 \] 
\vspace{-0.1in}
}


At line~\ref{line:design:settextC2}, the data is sent to a 
unspecified UI element and the line is typed with 
\typeinput(Line~$\ref{line:design:getinput}$). 
With rule Unexpected-Data-Use, we mark the corresponding data
generation point as 
$\mathit{MR}(\mbox{Line}~\ref{line:design:getinput}) -> True$, 
saying that the corresponding data generation point must be retained.


\subsection{Removing Code Elements} \label{sec:sub:rm}

\begin{figure*}[ht]
\centering\footnotesize
\begin{tabular}{|c|} \hline 
\begin{minipage}{3.15in}
\vspace{1mm}

\inference[Remove-Stmt-1]{\Gamma_1 |- l : \typeUI}
    {\Gamma_4 => [l : \typeR]\Gamma_4}
\vspace{2mm}

\inference[Unremove-Stmt]{\typeInput(l^d) \in \Gamma_3(l_\epsilon) &
    MR(l^d) & 
    \Gamma_1(l) \neq \typeUI}
    {\Gamma_4 => [l : \typeU]\Gamma_4}
\vspace{2mm}

\inference[Remove-Stmt-2]{\typeInput(l^d) \in \Gamma_3(l_\epsilon) &
    \neg MR(l^d)  &
    \Gamma_4(l) \neq \typeU }
    {\Gamma_4 => [l : \typeR]\Gamma_4}
\vspace{2mm}

\inference[Remove-Method]{nonApi(m) &
    \forall (x :=^l y.m(z)), \Gamma_4 |- l : \typeR }
    {\Gamma_4 => [m : \typeR]\Gamma_4}
\vspace{2mm}

\inference[Remove-Class]{nonFrameworkClass(k) &
    \forall (x :=^l new~k(y)), \Gamma_4 |- l : \typeR }
    {\Gamma_4 => [k : \typeR]\Gamma_4}
\vspace{2mm}

\inference[Remove-Stmts]{\Gamma_4 |- m : \typeR &
    l \in statementsIn(m) }
    {\Gamma_4 => [l : \typeR]\Gamma_4}
\vspace{2mm}

\inference[Remove-Methods]{\Gamma_4 |- k : \typeR &
    m \in methodsIn(k) }
    {\Gamma_4 => [m : \typeR]\Gamma_4}

\vspace{1mm}
\end{minipage} \\ \hline
\end{tabular}
\caption{Rules for removing code elements. }
\label{fig:rule-removal}
\vspace{-0.1in}
\end{figure*}

After we type all data correlated statements with one or more 
\typeInput types, we can finally determine which code 
elements are removable or unremovable (step \stepn{4}). 
The rules are shown in Figure~\ref{fig:rule-removal}.
We use $l$ to aggregate the code element in {\it all} calling contexts,
\ie $l_{\epsilon_j}$ for all $j$. 


Rule \rulename{Remove-Stmt-1} says, if a code element $l$ is directly related
to the specified UI elements in all possible calling contexts, it 
is absolutely removable. 
If $l$ in a context $\epsilon$ has a type of \typeInput which is 
associated with some data generation point $l^d$, and the data 
generation point needs to be retained when $l$ is not a UI related
code element, rule \rulename{Unremove-Stmt} declares $l$ as unremovable.
In contrast, if the corresponding data generation point is not 
required to be retained and the target $l$ has not been typed with
\typeU, we use \rulename{Remove-Stmt-2} to mark $l$ as removable. 
Rules \rulename{Remove-Method} and \rulename{Remove-Class} behave similarly. If all call
sites of a method or instantiation sites of a class are removed, 
the method or the class can be fully removed.
The last two rules iteratively remove included code elements when 
a method or a class is removable.

We are also able to find out the trigger sites of background 
functionalities, like the {\tt start()} call of a {\tt Thread} object 
or {\tt execute()} call on an {\tt AsyncTask} instance. If the 
triggered operations in background functionalities are all removable, 
we can remove the corresponding trigger sites as well.
Furthermore, we do not remove branch statements like {\tt if} and 
{\tt switch} even if they use variables whose definitions are
removable. Instead, we replace the definition statement of each such variable with a statement that 
assigns 0 or {\tt null} to that variable, depending on the type
of the variable. 


{\bf Example:} We can now apply the rules to remove code elements
in Figure~\ref{fig:design-code}. We have

{\footnotesize
\[\arraycolsep=2pt \left[ \begin{array}{rcl}
\mbox{Line~\ref{line:design:findviewB}, Line~\ref{line:design:settextB}, Line~\ref{line:design:findviewC1}, Line~\ref{line:design:settextC1}} & : & \typeR \\
\mbox{Line~\ref{line:design:settextC2}}, \mbox{Line~\ref{line:design:getinput}} & : & \typeU 
\end{array} \right] \Gamma_4 \] 
\vspace{-0.15in}
}

Therefore we can remove lines~\ref{line:design:findviewB}, 
\ref{line:design:settextB}, \ref{line:design:findviewC1} 
and \ref{line:design:settextC1}.
The background data generation point at line~\ref{line:design:getinput}
cannot be removed because its data flows to non-specified 
UI elements at line~\ref{line:design:settextC2}. 
However, under the context of class {\tt B} where 
the data is only used in the unwanted UI elements, we can disable
the corresponding trigger of the background functionality 
to avoid unnecessary network access after we remove the 
unwanted UI elements. We discover the corresponding trigger
site at line~\ref{line:design:triggerB} and we type it 
with \typeR while we retain the trigger site at 
line~\ref{line:design:triggerC}.

