=====================================

* paper.tex

	+  |-- abstract.tex

	+  |-- introduction.tex

	+  |-- motivation.tex

	+  |-- design.tex

	+  |-- evaluation.tex

	+  |-- related.tex

	+  |-- conclusion.tex
	
	+  |-- paper.bib
